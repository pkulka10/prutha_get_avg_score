## App Description ##

This repository contains code for a simple IGB App that calculates average score for the selected items for a track in IGB. This App can specifically be used for calculating average scores for ChIP-seq tracks.

### How do I run this app? ###

1. Install **Get Average Score** app.
2. Go back to IGB. 
3. Select a genome version in IGB. [You can use **A. thaliana** for demonstration]
4. Select a track file from options available in **Data Access**. [For demonstration - You can use **ChIP-seq** for **A. thaliana**. To use this track, select on **ChIP-seq** in **Data Access**. Click on **GNC PRJNA435725**. Click on **MACS2 peaks** in the options available. Select **GNC1 peaks**]
5. Click on **Load data** to load the track. 
6. Click and drag to select some items in the region for which you want to calculate the average score.
7. Select **Tools > Get Average Score** to run the App. The average score will appear in the dialog box.

### Questions or Comments? ###

Contact:

* Shamika Kulkarni - skulka26@uncc.edu or shamikagkulkarni@gmail.com
